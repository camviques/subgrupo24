package com.prueba.ws.dto;

import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlType(name = "responseGenerateTokenDto")
public class ResponseGenerateTokenDto {

	private String accessToken;

	@Override
	public String toString() {
		return "ResponseGenerateTokenDto [accessToken=" + accessToken + "]";
	}

}
