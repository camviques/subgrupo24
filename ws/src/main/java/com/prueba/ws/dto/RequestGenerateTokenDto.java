package com.prueba.ws.dto;

import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlType(propOrder = { "identificacion", "nombres", "apellidos" })
public class RequestGenerateTokenDto {

	private String nombres;
	private String apellidos;
	private String identificacion;

	@Override
	public String toString() {
		return "RequestGenerateTokenDto [nombres=" + nombres + ", apellidos=" + apellidos + ", identificacion="
				+ identificacion + "]";
	}

}
