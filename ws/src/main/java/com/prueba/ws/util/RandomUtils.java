package com.prueba.ws.util;

import java.util.Random;

public class RandomUtils {

	private RandomUtils() {
	}

	public static int generateRandomDigits(int n) {
		int m = (int) Math.pow(10, (n - 1));
		return m + new Random().nextInt(9 * m);
	}

	public static int generate8LengthDigit() {
		return generateRandomDigits(8);
	}

}
