package com.prueba.ws.service;

import com.prueba.ws.entity.Token;

public interface TokenService {

	public Token save(Token token);

	public String validarToken(String token);
}
