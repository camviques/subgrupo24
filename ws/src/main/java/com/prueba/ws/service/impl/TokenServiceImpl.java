package com.prueba.ws.service.impl;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.ws.entity.Token;
import com.prueba.ws.repository.TokenRepository;
import com.prueba.ws.service.TokenService;

@Service
public class TokenServiceImpl implements TokenService {

	private final TokenRepository tokenRepository;

	@Autowired
	private TokenServiceImpl(TokenRepository tokenRepository) {
		this.tokenRepository = tokenRepository;
	}

	@Override
	public Token save(Token token) {
		return this.tokenRepository.save(token);
	}

	@Override
	public String validarToken(String token) {
		Optional<Token> oToken = tokenRepository.findByAccessToken(token);

		if (!oToken.isPresent()) {
			return "No existe el token";
		}

		Token tokenEntity = oToken.get();
		if (LocalDateTime.now().isAfter(tokenEntity.getFechaVencimiento())) {
			return "Ya vencio el token";
		}

		return "Token Valido";
	}

}
