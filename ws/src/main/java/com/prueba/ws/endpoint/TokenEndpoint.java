package com.prueba.ws.endpoint;

import java.time.LocalDateTime;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prueba.ws.dto.RequestGenerateTokenDto;
import com.prueba.ws.dto.ResponseGenerateTokenDto;
import com.prueba.ws.entity.Token;
import com.prueba.ws.service.TokenService;
import com.prueba.ws.util.RandomUtils;

@WebService(serviceName = TokenEndpoint.SERVICE_NAME, targetNamespace = TokenEndpoint.TARGET_NAME_SPACE)
@Component
public class TokenEndpoint {

	public static final String SERVICE_NAME = "TokenServicio";
	public static final String TARGET_NAME_SPACE = "http://prueba.com/";

	private final TokenService tokenService;

	@Autowired
	private TokenEndpoint(TokenService tokenService) {
		this.tokenService = tokenService;
	}

	@WebMethod(operationName = "generateToken")
	public synchronized ResponseGenerateTokenDto generateToken(
			@WebParam(name = "requestGenerateTokenDto") RequestGenerateTokenDto request) {

		String token = String.valueOf(RandomUtils.generate8LengthDigit());

		Token tokenEntity = Token.builder().identificacion(request.getIdentificacion()).nombres(request.getNombres())
				.apellidos(request.getApellidos()).accessToken(token).fechaRegistro(LocalDateTime.now())
				.fechaVencimiento(LocalDateTime.now().plusMinutes(3)).build();

		this.tokenService.save(tokenEntity);
		return new ResponseGenerateTokenDto(token);
	}

	@WebMethod(operationName = "validateToken")
	public synchronized String validateToken(@WebParam(name = "token") String token) {
		return this.tokenService.validarToken(token);
	}

}
