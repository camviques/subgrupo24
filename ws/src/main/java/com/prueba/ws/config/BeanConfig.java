package com.prueba.ws.config;

import javax.servlet.Servlet;

import org.jvnet.jax_ws_commons.spring.SpringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.prueba.ws.endpoint.TokenEndpoint;
import com.sun.xml.ws.transport.http.servlet.SpringBinding;
import com.sun.xml.ws.transport.http.servlet.WSSpringServlet;

@Configuration
public class BeanConfig {

	@Autowired
	private TokenEndpoint tokenEndpoint;

	@Bean
	public ServletRegistrationBean<Servlet> servletRegistrationBean() {
		return new ServletRegistrationBean<>(new WSSpringServlet(), "/" + TokenEndpoint.SERVICE_NAME);
	}

	@Bean
	public SpringBinding binding() throws Exception {
		SpringService ss = new SpringService();
		ss.setBean(this.tokenEndpoint);
		SpringBinding r = new SpringBinding();
		r.setService(ss.getObject());
		r.setUrl("/" + TokenEndpoint.SERVICE_NAME);
		return r;
	}

}
