package com.prueba.ws.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "digit_token")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class Token {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String accessToken;
	private String identificacion;
	private String nombres;
	private String apellidos;
	private LocalDateTime fechaRegistro;
	private LocalDateTime fechaVencimiento;

	@Override
	public String toString() {
		return "Token [id=" + id + ", accessToken=" + accessToken + ", identificacion=" + identificacion + ", nombres="
				+ nombres + ", apellidos=" + apellidos + ", fechaRegistro=" + fechaRegistro + ", fechaVencimiento="
				+ fechaVencimiento + "]";
	}

}
