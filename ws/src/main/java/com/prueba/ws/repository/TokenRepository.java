package com.prueba.ws.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.ws.entity.Token;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

	public Optional<Token> findByAccessToken(String token);

}
